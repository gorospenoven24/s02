<?php require_once  "./code.php" ?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]>      <html class="no-js"> <!--<![endif]-->
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="#">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <h1>Repetition</h1>
        <h3>while loop</h3>
        <p><?php echo whileLoop();?></p>

        <h3>do while loop</h3>
        <p><?php echo doWhileLoop();?></p>

        <h3>for loop</h3>
        <p><?php echo forLoop();?></p>

        <h1>Array manipulation</h1>
        <h2> Types of arrays</h2>
        <h3>simple array</h3>
        <ul>
            <?php foreach($grades as $grade){?>
             <li><?php echo $grade;?></li> 
            <?php } ?>
        </ul>

        <h3>associative array</h3>
        <ul>
            <?php foreach($gradePeriods as $period => $grade){?>
             <li>Grade in <?= $period;?> is <?= $grade;?></li> 
            <?php } ?>
        </ul>

        <h3>Multi dimensional array</h3>
        <ul>
            <?php 
                foreach($heroes as $team){
                    foreach($team as $member) {
                    ?>
                    <li><?= $member?></li>
                    <?php
                    }
                }
            ?>
        </ul>

       <h3>Multi dimensional assoc array</h3>
       <?php
        foreach($powers as $label => $powerGroup){
            foreach($powerGroup as $power){?>
            <li><?= "$label: $power" ?></li>     
            <?php }
        }
       ?>

       <h1>Array functions</h1>
       <h3>add on last array</h3>
       <?php array_push($computerBrands, 'Apple'); ?>
       <pre><?php print_r($computerBrands);?></pre>
        
       <h3>add on 1st array</h3>
       <?php array_unshift($computerBrands, 'DELL'); ?>
       <pre><?php print_r($computerBrands);?></pre>

       <h3>remove last array</h3>
       <?php array_pop($computerBrands); ?>
       <pre><?php print_r($computerBrands);?></pre>

       
       <h3>remove 1st array</h3>
       <?php array_shift($computerBrands); ?>
       <pre><?php print_r($computerBrands);?></pre>

       <h3>sort/Reverse</h3>
       <?php sort($computerBrands); ?>
       <pre><?php print_r($computerBrands);?></pre>

       <?php rsort($computerBrands); ?>
       <pre><?php print_r($computerBrands);?></pre>

       <h3>Count</h3>
       <p><?php count($computerBrands);?></p>

       <h3>In array</h3>
       <p><?= searchBrand( 'HP' ,$computerBrands);?></p>
        <script src="" async defer></script>
    </body>
</html>