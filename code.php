<?php

function whileLoop() {
    $count = 0;
    while($count <=5){
        echo $count . '</br>';
        $count++;
    }
}

// Do while loop
function doWhileLoop(){
    $count = 20;
    do{
        echo $count . '</br>';
        $count--;
    }while($count > 0);
}

// For loop 
function forLoop(){
    for($i = 0; $i <=10; $i++){
        echo $i . "</br>";
    }
}

// [Section] array manipulation

$studentNumbers = array('1923', '1924', '1925', '1926');

$grades = [98.1, 89.2, 80.1, 90.1];

// Associative arrays
$gradePeriods = ['firstGrading' => 98.5, 'secondGrading' => 94.3, 'thirdGrading' => 89.2, 'fourthGrading' => 90.1];

// Multi- dimensionl arrays
$heroes = [
    ['Iron man', 'thor', 'hulk'],
    ['Wolvorine', 'cyclops', 'Jean Grey'],
    ['Batman', 'Superman', ' wonder woman']
];

// Multi-dimensional associative arrays

$powers = [
    'regular' => ['Repulsor blast', 'Rocket punch'],
    'speacial'=> ['hey']

];

$computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];

//searching arrays:
function searchBrand($brand, $brandsArr){
    if(in_array($brand, $brandsArr)){
        return "$brand Brand in arrays";
    } else {
        return "$brand brand is not array";
    }
}
